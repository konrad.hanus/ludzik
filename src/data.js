export default {
	values: [
		{
			name: 'Autentyczność',
			deleted: false
		},
		{
			name: 'Cierpliowść',
			deleted: false
		},
		{
			name: 'Duma',
			deleted: false
		},
		{
			name: 'Efektywność',
			deleted: false
		},
		{
			name: 'Hedonizm',
			deleted: false
		},
		{
			name: 'Kompetencja',
			deleted: false
		},
		{
			name: 'Mądrość',
			deleted: false
		},
		{
			name: 'Otwartość',
			deleted: false
		},
		{
			name: 'Piękno',
			deleted: false
		},
		{
			name: 'Prestiż',
			deleted: false
		},
		{
			name: 'Rodzina',
			deleted: false
		},
		{
			name: 'Rozrywka',
			deleted: false
		},
		{
			name: 'Sprawiedliwość',
			deleted: false
		},
		{
			name: 'Szczodrość',
			deleted: false
		},
		{
			name: 'Teolerancja',
			deleted: false
		},
		{
			name: 'Wiedza',
			deleted: false
		},
		{
			name: 'Wyrozumiałość',
			deleted: false
		},
		{
			name: 'Zaufanie',
			deleted: false
		},
		{
			name: 'Akceptacja',
			deleted: false
		},
		{
			name: 'Ciekawość',
			deleted: false
		},
		{
			name: 'Dyskrecja',
			deleted: false
		},
		{
			name: 'Entuzjazm',
			deleted: false
		},
		{
			name: 'Intuicja',
			deleted: false
		},
		{
			name: 'Lojalność',
			deleted: false
		},
		{
			name: 'Nadzieja',
			deleted: false
		},
		{
			name: 'Otoczenie',
			deleted: false
		},
		{
			name: 'Prawość',
			deleted: false
		},
		{
			name: 'Prawda',
			deleted: false
		},
		{
			name: 'Radość',
			deleted: false
		},
		{
			name: 'Równowaga',
			deleted: false
		},
		{
			name: 'Szczęśćie',
			deleted: false
		},
		{
			name: 'Spokój',
			deleted: false
		},
		{
			name: 'Troskliwość',
			deleted: false
		},
		{
			name: 'Wykształcenie',
			deleted: false
		},
		{
			name: 'Wsparcie',
			deleted: false
		},
		{
			name: 'Zaradność',
			deleted: false
		},
		{
			name: 'Bezpieczeństwo',
			deleted: false
		},
		{
			name: 'Czystość',
			deleted: false
		},
		{
			name: 'Duchowość',
			deleted: false
		},
		{
			name: 'Godność',
			deleted: false
		},
		{
			name: 'Konsekwencja',
			deleted: false
		},
		{
			name: 'Marzenia',
			deleted: false
		},
		{
			name: 'Niezależność',
			deleted: false
		},
		{
			name: 'Pasja',
			deleted: false
		},
		{
			name: 'Pomoc innym',
			deleted: false
		},
		{
			name: 'Pokora',
			deleted: false
		},
		{
			name: 'Rozwój',
			deleted: false
		},
		{
			name: 'Samoświadomość',
			deleted: false
		},
		{
			name: 'Stanowczość',
			deleted: false
		},
		{
			name: 'Stabilizacja',
			deleted: false
		},
		{
			name: 'Twórczość',
			deleted: false
		},
		{
			name: 'Wygoda',
			deleted: false
		},
		{
			name: 'Wrażliwość',
			deleted: false
		},
		{
			name: 'Zdrowie',
			deleted: false
		},
		{
			name: 'Bogactwo',
			deleted: false
		},
		{
			name: 'Czułość',
			deleted: false
		},
		{
			name: 'Doskonałość',
			deleted: false
		},
		{
			name: 'Harmonia',
			deleted: false
		},
		{
			name: 'Kreatywność',
			deleted: false
		},
		{
			name: 'Miłość',
			deleted: false
		},
		{
			name: 'Odpowiedzialność',
			deleted: false
		},
		{
			name: 'Przygoda',
			deleted: false
		},
		{
			name: 'Pogoda ducha',
			deleted: false
		},
		{
			name: 'Prostota',
			deleted: false
		},
		{
			name: 'Rozwaga',
			deleted: false
		},
		{
			name: 'Sława',
			deleted: false
		},
		{
			name: 'Sukces',
			deleted: false
		},
		{
			name: 'Szaucnek',
			deleted: false
		},
		{
			name: 'Uczciwość',
			deleted: false
		},
		{
			name: 'Wolność',
			deleted: false
		},
		{
			name: 'Wiarygodność',
			deleted: false
		},
		{
			name: 'Zadowolenie',
			deleted: false
		},
		{
			name: 'Bliskie relacje',
			deleted: false
		},
		{
			name: 'Delikatność',
			deleted: false
		},
		{
			name: 'Elastyczność',
			deleted: false
		},
		{
			name: 'Honor',
			deleted: false
		},
		{
			name: 'Komfort',
			deleted: false
		},
		{
			name: 'Misja',
			deleted: false
		},
		{
			name: 'Odwaga',
			deleted: false
		},
		{
			name: 'Perfekcjonizm',
			deleted: false
		},
		{
			name: 'Przyjaźń',
			deleted: false
		},
		{
			name: 'Przyjemność',
			deleted: false
		},
		{
			name: 'Ryzyko',
			deleted: false
		},
		{
			name: 'Spontaniczność',
			deleted: false
		},
		{
			name: 'Szczerość',
			deleted: false
		},
		{
			name: 'Szlachetność',
			deleted: false
		},
		{
			name: 'Wdzięczność',
			deleted: false
		},
		{
			name: 'Wierność',
			deleted: false
		},
		{
			name: 'Wiara',
			deleted: false
		},
		{
			name: 'Zaangażowanie',
			deleted: false
		}
	]
};