import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import data from './data.js';
import Stepper from './Stepper';

const useStyles = makeStyles((theme) => ({
	paper: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary
	}
}));

const levels = [15,15,15,15,0,10,5,4,3,0,0];
export default function FullWidthGrid() {
	const classes = useStyles();

	const [state, setState] = useState(data);
	const [counter, setCounter] = useState(15);
	const [level, setLevel] = useState(0);

	const onClick = (id) => {

		if (counter > 0) {
			const values = [...state.values];
			const value = { ...values[id] };
			value.deleted = !value.deleted;
			values[id] = value;
			setState({ values });

			if (value.deleted) {
				const newCounter = counter - 1;
				setCounter(newCounter);
			} else {
				const newCounter = counter + 1;
				setCounter(newCounter);
			}
		}
	};

	const next = () => {
		const values = state.values.filter((value)=> !value.deleted )
		setState({values: values});
		const nextLevel = level+1;
		setLevel(nextLevel);
		setCounter(levels[nextLevel]);
	}

	return (
		<div className={classes.root}>
			<Stepper next={next} counter={counter}/>
			{counter !==0 && <center><h3>Pozostało jeszcze {counter}</h3></center>}
			<Grid container spacing={1}>
				{state.values.map((value, key) => (
					<Grid item xs={6} sm={3} key={key} onClick={() => onClick(key)}>
						<Paper className={classes.paper}>{value.deleted ? <s>{value.name}</s> : value.name}</Paper>
					</Grid>
				))}
			</Grid>
		</div>
	);
}
