import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

function getSteps() {
    return ["Poziom 1", "Poziom 2", "Poziom 3", "Poziom 4", "Poziom 5", "Poziom 6", "Poziom 7", "Poziom 8", "Poziom 9", "Poziom 10", "Nagroda!"];
}

function getStepContent(stepIndex) {
    switch (stepIndex) {
        case 0:
            return 'Wykreśl 15 wartości, które czujesz, że są obecnie dla Ciebie najmniej ważne';
        case 1:
            return 'Wykreśl kolejne 15';
        case 2:
            return 'Znowu wykreśl 15';
        case 3:
            return 'Po raz ostatni wykreśl kolejne 15 wartości ';
        case 4:
            return 'Teraz zrób sobie chwilkę przerwy od ćwiczenia i zajmij się przez 5-10 minut czymś innym. Wyjdź na spacer, pooddychaj świeżym powietrzem albo rozprostuj kości i napij się wody. Chodzi tutaj o to, aby dać Twojemu mózgowi chwilę odpoczynku, ponieważ ćwiczenie zaczyna robić się coraz trudniejsze i wymaga coraz więcej skupienia.';
        case 5:
            return 'Skreśl kolejne 10 wartości';
        case 6:
            return 'Skreśl kolejne 5';
        case 7:
            return 'Wykreśl jeszcze 4';
        case 8:
            return 'I skreśl ostatnie 3. To najtrudniejsza część ćwiczenia, ponieważ teraz musisz wybrać między najważniejszymi dla siebie wartościami, ale zrób to. '
        case 9:
            return 'Zwróć teraz uwagę na swoje odczucia po tym ćwiczeniu — Zastanów się, co czujesz, patrząc na najważniejsze dla siebie życiowe wartości?'
        case 10:
            return 'Czas odebrać nagrodę! Narysuj na kartce ludzika. Przypożądkuj osiem wartości do części ciała, Głowa, Serce, Żołądek, Lewa ręka, Prawa ręka, Lewa Noga, Prawa Noga, Genitalia. Schowaj ludzika do portfela i noś swoje wartości zawsze przy sobie aby o nich pamiętać. Pomyśl, w jakim stopniu realizujesz swoje wartości je w tej chwili? Co możesz zrobić, aby lepiej o nie zadbać?';
        default:
            return 'Unknown stepIndex';
    }
}

export default function HorizontalLabelPositionBelowStepper(props) {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();

    const handleNext = () => {
        props.next();
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    return (
        <>
            <Grid item xs={12} >
                <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map((label) => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper>
            </Grid>
            <Grid item xs={12} container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center">
                {activeStep === steps.length ? (
                    <>
                        {/* <Typography className={classes.instructions}>All steps completed</Typography>
                        <Button onClick={handleReset}>Reset</Button> */}
                    </>
                ) : (
                        <>
                            <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                            {props.counter <= 0 &&
                               
                                    <Button variant="contained" color="primary" onClick={handleNext}>
                                        {activeStep === steps.length - 1 ? 'KONIEC :-)' : 'PRZEJDŹ NA KOLEJNY POZIOM'}
                                    </Button>
                              
                            }
                        </>
                    )}
            </Grid>
        </ >
    );
}